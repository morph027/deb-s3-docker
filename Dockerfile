FROM    ruby:alpine

RUN     apk --no-cache add \
            binutils \
            gnupg \
            bash

RUN     gem install \
            deb-s3 \
        && gem clean