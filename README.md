# Easily create and manage an APT repository on S3

[deb-s3](https://github.com/krobertson/deb-s3) in a container!

## Interactive

```
docker run --rm -it -v $PWD:/work -w /work registry.gitlab.com/morph027/deb-s3-docker:latest bash
```

## Non-interactive

```
docker run --rm -it -v $PWD:/work -w /work registry.gitlab.com/morph027/deb-s3-docker:latest deb-s3 upload --bucket ...
```